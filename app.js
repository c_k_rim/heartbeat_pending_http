const express = require('express');
const compression = require('compression');

const app = express();

app.use(compression());
app.use(express.urlencoded({extended: true}));
app.use(express.json());

const pool = []; //Пулл объектов response, которые, собственно, ожидают ответы

let counter = 0; //Какой-то счётчик. типа бизнес-логика

const heartbeat = async () => { //функция "тик" отрабатывает "каждую" секунду

    counter++; //Какая-то бизнес-логика

    while(pool.length) { //Пока пулл response не пуст
        pool.shift().json({heartbeat: counter}) //Забираем первый response, отдаём ему данные.
        // .json() это промис - всё должно быть довольно "оперативно" и асинхронно
    }

    await new Promise(resolve => setTimeout(resolve, 1000)); //Ждем секунду

    await heartbeat(); //заново запускаемся
}

//http://localhost:3000/
app.get('/', (_req, res) => res.sendFile(`${__dirname}/index.html`))

//Пришёл запрос get на эндпоинт /heartbeat
app.get('/heartbeat', (_req, res) => {
    pool.push(res); //запихиваем response в пулл. Функция heartbeat пользуется этим объектом, "когда надо"
})

app.listen(3000); //Слушаем порт 3000

heartbeat().catch(e => console.log(e)); //Запускаем рекурсию

console.log('http://localhost:3000/');